#include <iostream>
using namespace std;



int main(){
    double interest;
    int money, years;
    cout << "Amount of  money: ";
    cin >> money;
    cout << "Interest: ";
    cin >> interest;
    cout << "Years: ";
    cin >> years;
    for (int i=0; i<years; i++){
        money *= interest;
    }
    cout << "Final amount: " << money << endl;

return 0;
}
